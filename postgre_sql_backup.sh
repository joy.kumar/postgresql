#!/bin/bash

#Date and Time
date_time=$(date +'Date_%d-%m-%y_Time_%H:%M:%S')

#Backup Directory
backup_dir='/home/nasir/db_backup/postgre_sql/'

#
sudo pg_dump -U postgres -f "$backup_dir""wlama_db_$date_time".sql wlama_db

sudo pg_dump -U postgres -f "$backup_dir""mos_speedy_$date_time".sql mos_speedy

# Delete files older than 5 days
echo "Remove backup 30 days old"

find $backup_dir -mtime +30 -type f -print0 | xargs -0 rm -rf

#find $backup_dir/*.sql -mtime +30 -exec rm {} \;
