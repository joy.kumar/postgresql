# Backup and restore postgresql using barman
### Connection establishment setup with ssh authentication
* Installing backup `sudo apt-get update -y` and then execute `sudo apt-get install barman`. You should have postgresql in both `postgresql` and `barman` server.
* Go to `postgresql` server and then execute `sudo -i -u postgres` to enter postgres user in interactive mode. Create barman user with `createuser --interactive -P barman` command. Make it superuser when the prompt ask for that.
* go to `/etc/postgresql/[postgres-version-num]/postgresql.conf` and add `listen_addresses = '*'` if you want to add all ips. To mention specific IP add `listen_addresses = 'localhost, barman-server-ip'`
* Restart postgresql `sudo systemctl restart postgresql`
* Go to `/etc/postgresql/[postgres-version-num]/main` directory Add `host    postgres    barman    barman-server-ip    md5` in the `pg_hba.conf` file.
Example;
```
host    all             all             192.168.10.232/32       md5
``` 
* Go to `barman` server and execute `psql -c 'SELECT version()' -U barman -h postgresql-host-ip postgres`. Here, barman is the user and postgres is the name of database.
* Go to `barman` server and execute `sudo passwd barman` to add password, then enter the account `sudo -i -u barman`. Create pgpass file for barman user `echo "postgresql-host-ip:5432:*:barman:barman-password" >> ~/.pgpass`
* Connect to `barman` backup server, switch to barman user and execute `ssh-keygen -t rsa` and then copy the `id_rsa.pub` file to the `authorized_keys`  located in the .ssh directory of the postgres user of the postgres server.
* Copy the same pub key to the local `.ssh` directory of the postgres user of the barman server as postgresql is also available in barman server. Execute `ssh postgres@localhost -C true` command for sending data in compressed form.
* Login to postgres server as postgres user. Execute `ssh-keygen -t rsa` to generate key. Copy the generated public key (id_rsa.pub) to the barman user of the barman server. Test the connection from postgres server by executing `ssh barman@barman-server-host-ip -C true` 
### Configuring Barman
* Go to barman server.
* Create the directory `/etc/barman.d` by executing `mkdir /etc/barman.d` if it doesn't exist.
* Open the file `/etc/barman.conf` and remove the leading semicolon (**;**) from the following line:
`;configuration_files_directory = /etc/barman.d`
* Enter the directory `/etc/barman.d` and create a file `pgsql.conf` with the following content:
```
[pgsql]
description =  "Old PostgreSQL server"
conninfo = host=postgres-ip user=barman dbname=postgres
ssh_command = ssh postgres@postgres-ip
archiver=on
backup_method = rsync
retention_policy = RECOVERY WINDOW OF 7 DAYS
[pg]
description =  "Old PostgreSQL server2"
conninfo = host=postgres-ip user=barman dbname=erp_db
ssh_command = ssh postgres@postgres-ip
archiver=on
backup_method = rsync
retention_policy = RECOVERY WINDOW OF 7 DAYS
```
The information in this file is self-explanatory. The **retention_policy** defines the time frame during which backups are kept. This can be set to your needs. For more information about this file you may refer to the [official documentation](http://docs.pgbarman.org/release/2.4/#configuration).
### Configuring PostgreSQL
* Connect to `postgresql-serveer` and add this line to the file `/etc/postgresql/12/main/pg_hba.conf`. Here, 12 is the version number of postgresql.
`host all all barman-server-ip trust`
* Open the file `/etc/postgresql/[postgres-version-num]/main/postgresql.conf` and edit the values as shown:
```
wal_level = replica
archive_mode = on
archive_command = 'rsync -a %p barman@barman-server-ip:/var/lib/barman/pgsql/incoming/%f && rsync -a %p barman@barman-server-ip:/var/lib/barman/pg/incoming/%f' #if another db-backup is needed.Here pg is the configuration of another backup.
```
* Restart the postgresql service: `sudo systemctl restart postgresql`
* Check if the connection to the server `pgsql` works from Barman, by runnning the command `barman check pgsql` after logging into the **barman** user account on `barman-server`:
* `barman backup pgsql`  to backup postresql.
* `barman list-backup pgsql` to show all the backups available.
* `barman show-backup pgsql backup_id` to view all the backup details.

### Restoring Backups
* `sudo systemctl stop postgresql` command is needed to stop postgresql at the time of restoring.
* `barman recover --remote-ssh-command "ssh postgres@postgres-server-ip" pgsql 20230207T082740 /var/lib/postgresql/[postgres-version-num]/main`  \ Here, 20230207T082740 is the backup id which can be found from the execution of `barman list-backup pgsql`. This output will be shown if the backup is completed successfully.
* `sudo systemctl start postgresql` to start the postgres server again.

### Automatizing Backups
It is possible to run Barman automatically to backup the content of your database regularly. This can be done easily by setting up a cron job on `barman-server`.

1.  Switch into the **barman** user: `sudo -i -u barman`
2. Edit the crontab of the user: `crontab -e`
3. To run a backup daily at 3:30, add the following line to the cron tab and save it: `30 03 * * * /usr/bin/barman backup pgsql`

### Compatibility
* To backup latest postgresql you need latest barman. It means you need a barman application which is compatible with your postgresql. To install the latest version and read the documentation visit: [barman-official-site](https://dl.2ndquadrant.com/default/release/site/) 
* To install postgres visit: [postgresql-installation](https://www.postgresql.org/download/linux/ubuntu/)

Troubleshoot:
## troubleshooter for - error "WAL archive: FAILED (please make sure WAL shipping is setup)"

`barman switch-xlog --force --archive "$pg"
example: `barman switch-xlog --force --archive psql`


## Error and Fixed known Issues

Error: WAL archive: FAILED (please make sure WAL shipping is setup) \
```
Server replica:
WAL archive: FAILED (please make sure WAL shipping is setup)
PostgreSQL: OK
superuser: OK
wal_level: OK
directories: OK
retention policy settings: OK
backup maximum age: FAILED (interval provided: 1 day, latest backup age: No available backups)
compression settings: OK
failed backups: OK (there are 0 failed backups)
minimum redundancy requirements: FAILED (have 0 backups, expected at least 2)
ssh: OK (PostgreSQL server)
not in recovery: FAILED (cannot perform exclusive backup on a standby)
archive_mode: OK
archive_command: OK
continuous archiving: OK
archiver errors: OK
```

fixed: \
```sql
barman switch-xlog --force --archive replica
```


## Multi database Error and Fixed known issues:

Error: WAL archive: FAILED (please make sure WAL shipping is setup) \
```
Server replica:
WAL archive: FAILED (please make sure WAL shipping is setup)
PostgreSQL: Failed


Fixed:


`vim /etc/postgresql/[postgres-version-num]/main/postgresql.conf`

archive_command = 'rsync -a %p barman@barman-server-ip:/var/lib/barman/pgsql/incoming/%f && rsync -a %p barman@barman-server-ip:/var/lib/barman/pg/incoming/%f'